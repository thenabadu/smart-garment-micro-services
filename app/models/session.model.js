const mongoose = require('mongoose');

const SessionSchema = mongoose.Schema({
    sessionId:{ type: String, unique: true },
    sessionStatus:Number,
    userName:String,
    userGender:String,
    tshirtId:Number,
    tshirtSize:String,
    tShirtName:String,
    startTime:Date,
    validation:Boolean
});

module.exports = mongoose.model('session', SessionSchema);