const mongoose = require('mongoose');

const TrainingSensorDataSchema = mongoose.Schema({
    actionId:String,
    sessionId:String,
    validation:Boolean,
    timeStamp:Date,
    sensorList:[
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        },
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        },
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        },
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        },
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        },
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        },
        {
            acc:Array,
            gyr:Array,
            mag:Array,
            temperature:Number
        }
    ]
});

module.exports = mongoose.model('trainsensordata', TrainingSensorDataSchema);