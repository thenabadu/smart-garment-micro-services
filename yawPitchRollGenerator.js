"use strict";

class YawPitchRollGenerator {
  constructor(){
    this._q = [1,0,0,0];
    this._zeta = Math.sqrt(3.0 / 4.0) * Math.PI * (2.0 / 180.0);
    this._beta = Math.sqrt(3.0 / 4.0) * Math.PI * (40.0 / 180.0);
  }

  generate(ax,ay,az,gx,gy,gz,deltat){

    gx = gx*(Math.PI/180);
    gy = gy*(Math.PI/180);
    gz = gz*(Math.PI/180);

    let q1 = this._q[0], q2 = this._q[1], q3 = this._q[2], q4 = this._q[3];         // short name local variable for readability
    let norm;                                               // vector norm
    let f1, f2, f3;                                         // objetive funcyion elements
    let J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33; // objective function Jacobian elements
    let qDot1, qDot2, qDot3, qDot4;
    let hatDot1, hatDot2, hatDot3, hatDot4;
    let gerrx, gerry, gerrz, gbiasx, gbiasy, gbiasz;        // gyro bias error

    // Auxiliary variables to avoid repeated arithmetic
    let _halfq1 = 0.5 * q1;
    let _halfq2 = 0.5 * q2;
    let _halfq3 = 0.5 * q3;
    let _halfq4 = 0.5 * q4;
    let _2q1 = 2.0 * q1;
    let _2q2 = 2.0 * q2;
    let _2q3 = 2.0 * q3;
    let _2q4 = 2.0 * q4;
    let _2q1q3 = 2.0 * q1 * q3;
    let _2q3q4 = 2.0 * q3 * q4;

    // Normalise accelerometer measurement
    norm = Math.sqrt((ax * ax) + (ay * ay) + (az * az));
    if (norm == 0.0) return; // handle NaN
    norm = 1.0/norm;
    ax *= norm;
    ay *= norm;
    az *= norm;
    
    // Compute the objective function and Jacobian
    f1 = (_2q2 * q4) - (_2q1 * q3) - ax;//_2q2 * q4 - _2q1 * q3 - ax;
    f2 = (_2q1 * q2) + (_2q3 * q4) - ay;
    f3 = 1.0 - (_2q2 * q2) - (_2q3 * q3) - az;
    J_11or24 = _2q3;
    J_12or23 = _2q4;
    J_13or22 = _2q1;
    J_14or21 = _2q2;
    J_32 = 2.0 * J_14or21;
    J_33 = 2.0 * J_11or24;
  
    // Compute the gradient (matrix multiplication)
    hatDot1 = (J_14or21 * f2) - (J_11or24 * f1);
    hatDot2 = (J_12or23 * f1) + (J_13or22 * f2) - (J_32 * f3);
    hatDot3 = (J_12or23 * f2) - (J_33 *f3) - (J_13or22 * f1);
    hatDot4 = (J_14or21 * f1) + (J_11or24 * f2);
    
    // Normalize the gradient
    norm = Math.sqrt(hatDot1 * hatDot1 + hatDot2 * hatDot2 + hatDot3 * hatDot3 + hatDot4 * hatDot4);
    hatDot1 /= norm;
    hatDot2 /= norm;
    hatDot3 /= norm;
    hatDot4 /= norm;
    
    // Compute estimated gyroscope biases
    gerrx = (_2q1 * hatDot2) - (_2q2 * hatDot1) - (_2q3 * hatDot4) + (_2q4 * hatDot3);
    gerry = (_2q1 * hatDot3) + (_2q2 * hatDot4) - (_2q3 * hatDot1) - (_2q4 * hatDot2);
    gerrz = (_2q1 * hatDot4) - (_2q2 * hatDot3) + (_2q3 * hatDot2) - (_2q4 * hatDot1);
    
    // Compute and remove gyroscope biases
    gbiasx = gerrx * deltat * this._zeta;
    gbiasy = gerry * deltat * this._zeta;
    gbiasz = gerrz * deltat * this._zeta;
    gx -= gbiasx;
    gy -= gbiasy;
    gz -= gbiasz;
    
    // Compute the quaternion derivative
    qDot1 = (-1*_halfq2 * gx) - (_halfq3 * gy) - (_halfq4 * gz);
    qDot2 =  (_halfq1 * gx) + (_halfq3 * gz) - (_halfq4 * gy);
    qDot3 =  (_halfq1 * gy) - (_halfq2 * gz) + (_halfq4 * gx);
    qDot4 =  (_halfq1 * gz) + (_halfq2 * gy) - (_halfq3 * gx);

    // Compute then integrate estimated quaternion derivative
    q1 += (qDot1 -(this._beta * hatDot1)) * deltat;
    q2 += (qDot2 -(this._beta * hatDot2)) * deltat;
    q3 += (qDot3 -(this._beta * hatDot3)) * deltat;
    q4 += (qDot4 -(this._beta * hatDot4)) * deltat;

    // Normalize the quaternion
    norm = Math.sqrt((q1 * q1) + (q2 * q2)+ (q3 * q3) + (q4 * q4));    // normalise quaternion
    norm = 1.0/norm;
    this._q[0] = q1 * norm;
    this._q[1] = q2 * norm;
    this._q[2] = q3 * norm;
    this._q[3] = q4 * norm;
  }

  pitch(){
    let pitch = (Math.asin(2.0 * (this._q[1] * this._q[3] - this._q[0] * this._q[2])))*-1;
    pitch = pitch*(180/Math.PI);
    return pitch;
  }

  roll(){
    let roll = Math.atan2(2.0 * (this._q[0] * this._q[1] + this._q[2] * this._q[3]), this._q[0] * this._q[0] - this._q[1] * this._q[1] - this._q[2] * this._q[2] + this._q[3] * this._q[3]);
    roll = roll*(180/Math.PI);
    return roll;
  }
}

module.exports = YawPitchRollGenerator;