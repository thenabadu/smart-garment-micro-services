#!/usr/bin/env node
const yawPitchGenerator = require('./YawPitchRollGenerator');
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose'); 
const TrainingData = require('./app/models/trainingdata.model.js');
const Session = require('./app/models/session.model.js');
const Writetrainingdata = require('./app/models/writetrainingdata.model.js');

const consoleLogEnable = true;

mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

// Connecting to the database
mongoose.connect(dbConfig.url, {
	useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

Session.find({},{sessionId:1,startTime:1})
.then(sessions => {
    if(!sessions) {
        let messageTxt = `No session found...!`;
        if(consoleLogEnable){
            console.log(messageTxt);
            return;
        }
    }

    if(consoleLogEnable){
        console.log("Sessions found");
    }

    sessions.forEach(session => 
    {
        if(!session.sessionId)
        {
            return;
        }

        let yawPitchGenerators=[];
        let previousTime = 0;
        for(let i=0;i<7;i++)
        {
            yawPitchGenerators[i] = new yawPitchGenerator();
        }
        
        TrainingData.find({sessionId:session.sessionId}).sort({timeStamp:1})
        .then(trainingDatas => {
            if(!trainingDatas) {
                let messageTxt = `No training sensor data found...!`;
                if(consoleLogEnable){
                    console.log(messageTxt);
                    return;
                }
            }
        
            if(consoleLogEnable){
                console.log("found training sensor data list");
            }
        
            trainingDatas.forEach(trainingData => {
                let generatedWritetrainingdata = {
                    actionId:trainingData.actionId,
                    sessionId:trainingData.sessionId,
                    validation:trainingData.validation,
                    timeStamp:trainingData.timeStamp,
                    sensorList:[]
                }
    
                if(previousTime==0){
                    previousTime = trainingData.timeStamp;
                }
                let timeDiff = trainingData.timeStamp - previousTime;

                for(let i=0;i<7;i++)
                {
                    let sensorObj = { 
                        acc:trainingData.sensorList[i].acc,
                        gyr:trainingData.sensorList[i].gyr,
                        mag:trainingData.sensorList[i].mag,
                        temperature:trainingData.sensorList[i].temperature
                    };

                    generatedWritetrainingdata.sensorList[i] = sensorObj;
                    let ax = trainingData.sensorList[i].acc[0];
                    let ay = trainingData.sensorList[i].acc[1];
                    let az = trainingData.sensorList[i].acc[2];

                    let gx = trainingData.sensorList[i].gyr[0];
                    let gy = trainingData.sensorList[i].gyr[1];
                    let gz = trainingData.sensorList[i].gyr[2];

                    yawPitchGenerators[i].generate(ax,ay,az,gx,gy,gz,timeDiff);
                    generatedWritetrainingdata.sensorList[i].pitch = yawPitchGenerators[i].pitch();
                    generatedWritetrainingdata.sensorList[i].roll = yawPitchGenerators[i].roll();
                }
                const writeTrainingSensorData = new Writetrainingdata(generatedWritetrainingdata);
                // Save Activity in the database
                writeTrainingSensorData.save()
                .then(act => {
                    if(consoleLogEnable){
                        console.log("successfuly add new sensor data to modifytrainsensordata Collection");
                    }
                }).catch(err => {
                    let messageTxt = "Some error occurred while adding new sensor data to modifytrainsensordata Collection";
                    if(consoleLogEnable){
                        console.log(messageTxt);
                    }
                });
            });  
            
        }).catch(err => {
            let messageTxt = "Some error occurred while processing training sensor data";
            if(consoleLogEnable){
                console.log(messageTxt);
            }
            return;
        });

    });

}).catch(err => {
    
    if(consoleLogEnable){
        console.log("error occured, cannot find sessions");
    }
});