const mongoServerLocation = "192.168.3.155";
const mongoPort = "27017";
const dbName = "SmartGarmentTest";

const url = 'mongodb://' + mongoServerLocation + ':' + mongoPort + '/' + dbName;
module.exports = {
    url: url
}
